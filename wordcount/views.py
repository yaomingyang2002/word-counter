from django.http import HttpResponse
from django.shortcuts import render
import operator

def home(request):
    return render(request, 'home.html')

def count(request):
    fulltext = request.GET['fulltext'] # get the textarea content

    wordlist=fulltext.split() #generate a wordlist

    #count individual word
    worddict={}

    for word in wordlist:
        if word in worddict:
            #increase
            worddict[word] +=1
        else:
            #add to the dict
            worddict[word]=1

    sortedword=sorted(worddict.items(), key=operator.itemgetter(1), reverse=True)

    return render(request, 'count.html', {'fulltext':fulltext, 'totalcount':len(wordlist), 'worddict':worddict.items(), 'sortedword':sortedword}) #pass to the dict

def about(request):
    return render(request, 'about.html')