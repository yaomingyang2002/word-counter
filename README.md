This word counter is a simple web application created with Python - Django 2.0.2 framework

User can enter text, click count, and get total count and word count.

The step-by-step commits show the development work flow.

This project will be helpful for learning and refreshing python and Django 2.